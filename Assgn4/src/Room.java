public class Room {
/**
 * wallColor is the color of the wall.
 * windows is the number of the windows in the room.
 * floor is the floor material type.
 */
	private String wallColor;
	private int windows;
	private String floor;

/**
 * Create a new room with no description.
 */
	public Room() {
	this.wallColor = "not colored";
	this.windows = 0;
	this.floor = "not defined";
	}

/**
 * Create a new room with three information.
 * @param wallColor is the color of the wall.
 * @param windows is the number of the windows in the room.
 * @param floor is the floor material type.
 */
	public Room(String wallColor, int windows, String floor) {
	this.wallColor = wallColor;
	this.windows = windows;
	this.floor = floor;
	}
	
	/**
	 * @return the wall color from the room description. 
	 */
	public String getWallColor() {return wallColor;}
	/**
	 * @return the number of windows from the room description.
	 */
	public int getWindows() {return windows;}
	/**
	 * @return the floor type from the room description.
	 */
	public String getFloor() {return floor;}
/**
 * To set the wall color.
 * @param wallColor is the color of the wall.
 */
	public void setWallColor(String wallColor) {this.wallColor = wallColor;}
/**
 * To set the number of the windows in the room.
 * @param windows is the number of the windows in the room.
 */
	public void setWindows(int windows) {this.windows = windows;}
/**
 * To set the floor type.
 * @param floor is the floor material type
 */
	public void setFloor(String floor) {this.floor = floor;}
/**
 * To return the details about the room.
 */
	public String toString() {
		return "has "+ getWindows() + " window(s) with "+ getWallColor() 
				+ " wall and " + getFloor() + " floor.";}
}
