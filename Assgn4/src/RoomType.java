//Create a customized class and methods
public class RoomType {

	public static void main(String[] args) {
		//Create 3 new objects, named room1, room2, and room3 with its details 
			Room room1 = new Room("Yellow", 1,"hard wood");
			Room room2 = new Room("Purple", 0,"tile");
			Room room3 = new Room();
			room3.setWallColor("white"); //set the wall color for room 3	
			room3.setWindows(3); 	 //set the number of windows for room 3
			room3.setFloor("carpet");	 //set the floor type for room 3
		//Print the description for each room
			System.out.println("room1 " + room1);
			System.out.println("room2 " + room2);
			System.out.println("room3 " + room3);
			}
	}
